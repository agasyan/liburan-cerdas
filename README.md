# liburan-cerdas

Liburan Cerdas (atau disingkat LC) adalah suatu layanan berbasis website sebagai proyek akhir mata kuliah SC Genap 2018/2019

## Nama dan Anggota Kelompok

Nama Kelompok: ShinoChika

- Agas Yanpratama - 1606918396
- Mohammad Ammar Ramadhan - 1606918591
- Ervan A. Hariyadi - 1706074871

## Website

<http://liburan-cerdas.herokuapp.com/>

## Poster

![SIWISATA Poster](https://gitlab.com/agasyan/liburan-cerdas/raw/master/poster/Poster_SC.png "Liburan Cerdas Poster")

## Data Sets

_Data Crawling_ dari _google maps_ dan _Wikipedia_.

* * *

## Presentation

<http://bit.ly/liburan-cerdas-ppt>