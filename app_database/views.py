from app_home.models import Restaurant, TempatWisata, Hotel
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here

response = {}


def add_dot(my_str, group=3, char='.'):
    my_str = str(my_str)[::-1]
    res = char.join(my_str[i:i+group] for i in range(0, len(my_str), group))
    res_normal = ''.join(res)
    result = res_normal[::-1]
    return result


def index(request):
    response.clear()
    response['pageTitle'] = 'Database Home'
    response['activeDatabase'] = 'active'
    return render(request, 'index_db.html', {'response': response})


def restoran_view(request):
    page = request.GET.get('page', 1)
    response.clear()
    response['pageTitle'] = 'List Restoran'
    response['activeDatabase'] = 'active'
    restoran_list = Restaurant.objects.all().order_by('id')
    for i in restoran_list:
        i.price = add_dot(i.price)
        i.price = "Rp" + i.price + ",00"
    if(restoran_list.count() == 0):
        response['nothing'] = 'True'
    paginator = Paginator(restoran_list, 6)
    try:
        restorans = paginator.page(page)
    except PageNotAnInteger:
        restorans = paginator.page(1)
    except EmptyPage:
        restorans = paginator.page(paginator.num_pages)
    return render(request, 'restoran.html', {'restorans': restorans, 'response': response})


def hotel_view(request):
    page = request.GET.get('page', 1)
    response.clear()
    response['pageTitle'] = 'List Hotel'
    response['activeDatabase'] = 'active'
    hotel_list = Hotel.objects.all().order_by('id')
    for i in hotel_list:
        i.price = add_dot(i.price)
        i.price = "Rp" + i.price + ",00"
    if(hotel_list.count() == 0):
        response['nothing'] = 'True'
    paginator = Paginator(hotel_list, 6)
    try:
        hotels = paginator.page(page)
    except PageNotAnInteger:
        hotels = paginator.page(1)
    except EmptyPage:
        hotels = paginator.page(paginator.num_pages)
    return render(request, 'hotel.html', {'hotels': hotels, 'response': response})


def tempat_wisata_view(request):
    page = request.GET.get('page', 1)
    response.clear()
    response['pageTitle'] = 'List Tempat Wisata'
    response['activeDatabase'] = 'active'
    attraction_list = TempatWisata.objects.all().order_by('id')
    for i in attraction_list:
        i.price_min = add_dot(i.price_min)
        i.price_min = "Rp" + i.price_min + ",00"
        i.price_max = add_dot(i.price_max)
        i.price_max = "Rp" + i.price_max + ",00"
    if(attraction_list.count() == 0):
        response['nothing'] = 'True'
    paginator = Paginator(attraction_list, 6)
    try:
        attractions = paginator.page(page)
    except PageNotAnInteger:
        attractions = paginator.page(1)
    except EmptyPage:
        attractions = paginator.page(paginator.num_pages)
    return render(request, 'tempat_wisata.html', {'attractions': attractions, 'response': response})
