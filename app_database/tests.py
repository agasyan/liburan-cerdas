from django.test import TestCase, Client
from .views import add_dot
from app_home.tests import (create_hotel as ch, create_restoran as cr,
                            create_tempat_wisata as ctw)


# Create your tests here.


class app_databaseUnitTest(TestCase):

    def test_landing_page_exist(self):
        response = Client().get('/database/')
        self.assertEqual(response.status_code, 200)

    def test_negative_landing(self):
        response = Client().get('/databaseeee/')
        self.assertEqual(response.status_code, 404)

    def test_restoran_page_exist(self):
        response = Client().get('/database/restoran/')
        self.assertEqual(response.status_code, 200)

    def test_negative_restoran(self):
        response = Client().get('/databaseeee/')
        self.assertEqual(response.status_code, 404)

    def test_nothing_restoran_page(self):
        response = Client().get('/database/restoran/')
        self.assertContains(response, 'Tidak ada Restoran yang terdaftar!')

    def test_nothing_hotel_page(self):
        response = Client().get('/database/hotel/')
        self.assertContains(response, 'Tidak ada Hotel yang terdaftar!')

    def test_nothing_attraction_page(self):
        response = Client().get('/database/attraction/')
        self.assertContains(response, 'Tidak ada Tempat Wisata yang terdaftar!')

    def test_add_dot(self):
        str = "200000"
        new_str = add_dot(str)
        expected = "200.000"
        self.assertEqual(new_str, expected)

    def test_hotel_page_exist(self):
        response = Client().get('/database/hotel/')
        self.assertEqual(response.status_code, 200)

    def test_tempat_wisata_page_exist(self):
        response = Client().get('/database/attraction/')
        self.assertEqual(response.status_code, 200)

    def test_restoran_page_with_data(self):
        cr('mcd')
        response = Client().get('/database/restoran/')
        self.assertEqual(response.status_code, 200)

    def test_hotel_page_with_data(self):
        ch('hi')
        response = Client().get('/database/hotel/')
        self.assertEqual(response.status_code, 200)

    def test_tempat_wisata_page_with_data(self):
        ctw('monas')
        response = Client().get('/database/attraction/')
        self.assertEqual(response.status_code, 200)

    def test_tempat_wisata_page_pagination(self):
        ctw('monas')
        response = Client().get('/database/attraction/?page=mesaaas')
        response1 = Client().get('/database/attraction/?page=1')
        self.assertEqual(response.content, response1.content)

    def test_tempat_wisata_page_empty(self):
        ctw('monas')
        response = Client().get('/database/attraction/?page=100000')
        response1 = Client().get('/database/attraction/?page=1')
        self.assertEqual(response.content, response1.content)

    def test_restoran_page_pagination(self):
        cr('mcd')
        response = Client().get('/database/restoran/?page=mesaaas')
        response1 = Client().get('/database/restoran/?page=1')
        self.assertEqual(response.content, response1.content)

    def test_restoran_page_empty(self):
        cr('md')
        response = Client().get('/database/restoran/?page=100000')
        response1 = Client().get('/database/restoran/?page=1')
        self.assertEqual(response.content, response1.content)

    def test_hotel_page_pagination(self):
        cr('hyatt')
        response = Client().get('/database/hotel/?page=mesaaas')
        response1 = Client().get('/database/hotel/?page=1')
        self.assertEqual(response.content, response1.content)

    def test_hotel_page_empty(self):
        cr('hyatt')
        response = Client().get('/database/hotel/?page=100000')
        response1 = Client().get('/database/hotel/?page=1')
        self.assertEqual(response.content, response1.content)
