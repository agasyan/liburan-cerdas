from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('restoran/', views.restoran_view, name='restoran_view'),
    path('hotel/', views.hotel_view, name='hotel_view'),
    path('attraction/', views.tempat_wisata_view, name='tempat_wisata_view')
]
