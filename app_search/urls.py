from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('submit/', views.search, name='search'),
    # path('repeat/', views.resetPlan, name='repeat'),
]
