from app_home.models import Restaurant, TempatWisata, Hotel
from django.shortcuts import render, redirect
from math import sin, cos, sqrt, atan2, radians
from datetime import datetime, timedelta
from urllib.request import urlopen, Request
import json
from app_database.views import add_dot

# Create your views here.

response = {}
places = []                     # keeps places that will be visited
traveling = list()              # keeps travel data (['distance'], ['duration'], ['cost'])
tempInitStartingPoint = {}      # used for repeat with same input

lunchStartStr = '10:45'
lunchEndStr = '14:15'
dinnerStartStr = '18:45'        # kalo lewat gak usah makan
dinnerEndStr = '20:15'
lunchStart = datetime.strptime(lunchStartStr, '%H:%M')
lunchEnd = datetime.strptime(lunchEndStr, '%H:%M')
dinnerStart = datetime.strptime(dinnerStartStr, '%H:%M')
dinnerEnd = datetime.strptime(dinnerEndStr, '%H:%M')


# API for counting the distance & time to reach destinations
# https://api.tomtom.com/routing/1/calculateRoute/-6.3192632,106.8757177:-6.1784541,106.8307066/json?avoid=unpavedRoads&key=I5Fy8xKqAAmzAutF0pHOWmKFhccV6dwF
def get_distance_time(lat_init, lon_init, lat_end, lon_end):
    tomtom_part1 = "https://api.tomtom.com/routing/1/calculateRoute/"
    tomtom_part2 = "/json?avoid=unpavedRoads&key=I5Fy8xKqAAmzAutF0pHOWmKFhccV6dwF"
    lat_init_str = str(lat_init)
    lon_init_str = str(lon_init)
    lat_end_str = str(lat_end)
    lon_end_str = str(lon_end)
    url = (tomtom_part1 + lat_init_str + "," + lon_init_str + ":" + lat_end_str + "," + lon_end_str +
           tomtom_part2)
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64)'
           'AppleWebKit/537.11 (KHTML, like Gecko) '
           'Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'json',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    req = Request(url=url, headers=hdr)
    url = urlopen(req).read()
    data = json.loads(url)
    out = {}
    out['time_sec'] = data['routes'][0]['summary']['travelTimeInSeconds']
    out['distance_meter'] = data['routes'][0]['summary']['lengthInMeters']
    return out


# API for counting the distance & time to reach destinations manual
def get_distance_time_manual(lat_init, lon_init, lat_end, lon_end, transport_type):
    lat_init_float = float(lat_init)
    lon_init_float = float(lon_init)
    lat_end_float = float(lat_end)
    lon_end_float = float(lon_end)
    meter = int(euclidian_distance_meter(lat_init_float, lon_init_float, lat_end_float, lon_end_float)) * 1.5
    int_meter = int(meter)
    # Asumsi 18 km/jam = 5 m/s -> Asumsi Waktu jakarta Mobil
    # Asumsi 27 km/jam = 7.5 m/s -> Motor
    out = {}
    if transport_type == 'gojek':
        out['time_sec'] = int(int_meter/7.5)
    else:
        out['time_sec'] = int(int_meter/5)
    out['distance_meter'] = int_meter
    return out


# approximate radius of earth in km
def euclidian_distance_meter(lat_init, lon_init, lat_end, lon_end):
    R = 6373.0
    lat1r = radians(lat_init)
    lon1r = radians(lon_init)
    lat2r = radians(lat_end)
    lon2r = radians(lon_end)
    dlon = lon2r - lon1r
    dlat = lat2r - lat1r
    a = sin(dlat / 2)**2 + cos(lat1r) * cos(lat2r) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c * 1000
    int_dist = int(distance)
    return int_dist


def index(request):
    response.clear()
    response['pageTitle'] = 'Search Form'
    response['activeSearch'] = 'active'
    return render(request, 'search.html', {'response': response})


def search(request):
    response.clear()
    if request.method == 'POST':
        # Semua dalam bentuk str
        init_place = {}
        init_place['start_point'] = request.POST['startPoint']
        init_place['pos_lat'] = request.POST['lat-init']
        init_place['pos_lon'] = request.POST['long-init']
        init_place['menginap'] = request.POST['seeAnotherField']
        init_place['bintang'] = request.POST['otherField']
        init_place['tanggal'] = request.POST['tanggal']
        init_place['jam_init'] = request.POST['jam-awal']
        init_place['jam_end'] = request.POST['jam-akhir']
        init_place['transport'] = request.POST['transport']
        # tempInitStartingPoint = init_place            # should be used as init for re-mapping (if implemented)

        getPlannedTrip(init_place, init_place['transport'])

        trip_list = place_to_trip(places, init_place['transport'])
        response['pageTitle'] = 'Result'
        response['activeSearch'] = 'active'
        return render(request, 'result.html', {'trips': trip_list[0], 'response': response, 'input': init_place,
                                               'price': trip_list[1], 'price_max': trip_list[2]})
    return redirect('/search')


# convert 12H to 24H as String
def clock_24h_from_12(clock):
    d = datetime.strptime(clock, ("%I:%M %p"))
    d_new = d.strftime("%H:%M")
    return d_new


# Cara ambil nama hari = out['nama'] atau id = out['id']
def get_day_from_date_indonesia(str_date):
    day = datetime.strptime(str_date, '%m/%d/%Y').strftime('%A')
    out = {}
    if day == "Sunday":
        out['nama'] = 'Minggu'
        out['id'] = 7
    elif day == "Monday":
        out['nama'] = 'Senin'
        out['id'] = 1
    elif day == "Tuesday":
        out['nama'] = 'Selasa'
        out['id'] = 2
    elif day == "Wednesday":
        out['nama'] = 'Rabu'
        out['id'] = 3
    elif day == "Thursday":
        out['nama'] = 'Kamis'
        out['id'] = 4
    elif day == "Friday":
        out['nama'] = 'Jumat'
        out['id'] = 5
    elif day == "Saturday":
        out['nama'] = 'Sabtu'
        out['id'] = 6
    return out


# get the cost for transporting between each attractions
def fare_transport(type, dist_meter):
    fare = 0
    if type == "gojek":
        # 2200 per kilometer
        fare = dist_meter*2.2
    elif type == "gocar":
        # 3600 per kilometer
        fare = dist_meter*3.6
    elif type == "taxi":
        # 4100 per kilometer
        fare = dist_meter*4.1 + 6500
    fare = int(fare)
    fare_div = fare//1000
    fare_mod = fare % 1000
    if (fare_mod > 499):
        fare = (fare_div + 1)*1000
    else:
        fare = fare_div*1000
    return fare


def getHour(time):
    return int(time.split(":")[0])


# returns time in DateTime format from String
def getTimeFormat(timeStr):
    return datetime.strptime(timeStr, ("%H:%M"))


# all input as str
# return an updated time caused by trip and attraction in str
def timeUpdater(initTimeStr, travelInt, spentTimeInt):
    initTime = datetime.strptime(initTimeStr, ("%H:%M"))
    travel = timedelta(minutes=travelInt)
    spentTime = timedelta(minutes=spentTimeInt)
    timeDiff = initTime + travel + spentTime                    # as datetime
    return timeDiff.time().strftime("%H:%M")                    # return the time only, as TIME format


# # reset itienary
# def resetPlan(request):
#     getPlannedTrip(tempInitStartingPoint)
#     trip_list = place_to_trip(places, tempInitStartingPoint['transport'])
#     response['pageTitle'] = 'Result'
#     response['activeSearch'] = 'active'
#     return render(request, 'result.html', {'trips': trip_list[0], 'response': response,
#                                            'input': tempInitStartingPoint, 'price': trip_list[1],
#                                            'price_max': trip_list[2]})


# get the duration of the travel in minutes (integer)
def getDuration(start, end):
    startTime = datetime.strptime(start, ("%H:%M"))
    endTime = datetime.strptime(end, ("%H:%M"))
    return (endTime - startTime).seconds//60        # get minute


# check if the User wants to stay overnight
def isInap(inapStr):
    if (inapStr == 'yes'):
        return 120
    else:
        return 0


# check if the location is closed for the day
def isPlaceClosed(querysets, today):
    for holiday in querysets:
        if (holiday.day_name == today):
            return True
    return False


# check if the location has been included to itienary
def isVisitedBefore(place):
    if place in places:
        return True
    return False


def isCurrentlyClose(place, today, currentTime, travelTime):
    current = timeUpdater(currentTime, travelTime, 0)
    startTimeInt = getHour(current)
    current = timeUpdater(currentTime, travelTime, place.time_minute)
    endTimeInt = getHour(current)
    if (0 < today['id'] < 6):
        if (place.weekday_from_hour <= startTimeInt <= place.weekday_to_hour):
            if (endTimeInt <= place.weekday_to_hour):
                return False
    else:
        if (place.weekend_from_hour <= startTimeInt <= place.weekend_to_hour):
            if (endTimeInt <= place.weekend_to_hour):
                return False
    return True


def getPlannedTrip(init, transport_type):
    # define initial values
    places.clear()
    traveling.clear()
    hadLunch = False
    hadDinner = False
    lat = float(init['pos_lat'])
    lon = float(init['pos_lon'])
    start24 = clock_24h_from_12(init['jam_init'])
    end24 = clock_24h_from_12(init['jam_end'])
    today = get_day_from_date_indonesia(init['tanggal'])
    timeThreshold = isInap(init['menginap'])                # threshold for traveling to hotel
    customerTime = getDuration(start24, end24)              # itienary duration in minutes

    # add initial place & time to list
    places.append(init)
    traveling.append(start24)

    # give 2 hours time to reach hotel (if staying). If not, spend all time
    while (customerTime > timeThreshold):
        place = None
        distance = None
        travel = None
        resto = None
        if ((lunchStart <= getTimeFormat(traveling[-1]) <= lunchEnd) & (not hadLunch)):
            while (resto is None):
                resto = getRestaurant(lat, lon, today, traveling[-1], customerTime, transport_type)
            place = resto['resto']
            travel = resto['travel']
            hadLunch = True
        elif ((dinnerStart <= getTimeFormat(traveling[-1]) <= dinnerEnd) & (not hadDinner)):
            resto = getRestaurant(lat, lon, today, traveling[-1], customerTime, transport_type)
            if (resto is None):
                place = None
            else:
                place = resto['resto']
                travel = resto['travel']
                # print('ada restoran')
                hadDinner = True
        else:
            tempatWisata = TempatWisata.objects.order_by('?')[:15]
            # iterate through the selected attractions
            for tempat in tempatWisata:
                # if the selected place was closed for the day
                if (isPlaceClosed(tempat.closed_day.all(), today['nama'])):
                    continue
                # if place is currently empty
                if (place is None):
                    if ((not isVisitedBefore(tempat))):
                        place = tempat
                        distance = euclidian_distance_meter(lat, lon, tempat.pos_lat, tempat.pos_lon)
                    else:
                        continue
                elif ((not isVisitedBefore(tempat))):
                    tempDist = euclidian_distance_meter(lat, lon, tempat.pos_lat, tempat.pos_lon)
                    if (tempDist < distance):
                        place = tempat
                        distance = tempDist
                else:
                    continue
                travel = get_distance_time_manual(lat, lon, place.pos_lat, place.pos_lon, transport_type)
                temptime = (customerTime - travel['time_sec']//60 - place.time_minute)
                # print('ada tempat wisata {}'.format(traveling[-1]))
                if (temptime < 0 or isCurrentlyClose(place, today, traveling[-1], travel['time_sec']//60)):
                    place = distance = None

        if(place is None):
            break

        travelTime = travel['time_sec']//60                             # time in minutes
        # travelDistance = travel['distance_meter']                       # distance in meters
        time = timeUpdater(start24, travelTime, place.time_minute)
        customerTime -= (travelTime + place.time_minute)
        # destinasi berikutnya sudah didapatkan, lanjut ke masukkan ke list
        # update current latitude, longitude & time
        lat = place.pos_lat
        lon = place.pos_lon
        start24 = time

        places.append(place)
        traveling.append(start24)

    if (isInap(init['menginap'])):
        getHotel(lat, lon, start24, init['bintang'], transport_type)  # getHotel do everything on hotel-based search
        # print('ada hotel')

    # printList(places)
    # printList(traveling)


# def printList(aList):
#     for a in aList:
#         print(a)
#     print(" ")


def getRestaurant(pos_lat, pos_lon, today, currentTimeStr, timeLeft, transport_type):
    tempResto = None
    restoran = {}
    restoran['resto'] = None
    restoran['travel'] = None
    restaurantList = Restaurant.objects.order_by('?')[:10]
    for resto in restaurantList:
        # if the selected place was closed for the day
        if (isPlaceClosed(resto.closed_day.all(), today['nama'])):
            continue
        # if place is currently empty
        if (tempResto is None):
            if (not isVisitedBefore(resto)):
                tempResto = resto
                distance = euclidian_distance_meter(pos_lat, pos_lon, resto.pos_lat, resto.pos_lon)
        elif (not isVisitedBefore(resto)):
            tempDist = euclidian_distance_meter(pos_lat, pos_lon, resto.pos_lat, resto.pos_lon)
            if (tempDist < distance):
                tempResto = resto
                distance = tempDist

        travel = get_distance_time_manual(pos_lat, pos_lon, tempResto.pos_lat, tempResto.pos_lon, transport_type)
        temptime = (timeLeft - travel['time_sec']//60 - tempResto.time_minute)
        if (temptime < 0 or isCurrentlyClose(tempResto, today, traveling[-1], travel['time_sec']//60)):
            # print(temptime, isCurrentlyClose(tempResto, today, traveling[-1], travel['time_sec']//60))
            tempResto = travel = None
        else:
            restoran['resto'] = tempResto   # place
            restoran['travel'] = travel     # distance
            # print(temptime, isCurrentlyClose(tempResto, today, traveling[-1], travel['time_sec']//60))
        # print(resto, traveling[-1])
    return restoran


def getStar(star):
    starArr = star.split(',')
    starArrInt = list()
    for i in starArr:
        starArrInt.append(int(i))
    return starArrInt


def getHotel(lat, lon, start24, star, transport_type):
    starArr = getStar(star)
    hotelList = Hotel.objects.filter(hotel_star__in=starArr)
    place = None
    distance = None
    for hotel in hotelList:
        if (place is None):
            place = hotel
            distance = euclidian_distance_meter(lat, lon, hotel.pos_lat, hotel.pos_lon)
        else:
            tempDist = euclidian_distance_meter(lat, lon, hotel.pos_lat, hotel.pos_lon)
            if (tempDist < distance):
                place = hotel
                distance = tempDist

    travel = get_distance_time_manual(lat, lon, place.pos_lat, place.pos_lon, transport_type)
    travelTime = travel['time_sec']//60                             # time in minutes
    # travelDistance = travel['distance_meter']                       # distance in meters

    time = timeUpdater(start24, travelTime, 0)

    places.append(place)
    traveling.append(time)


def add_time_to_clock(str_time, int_minute):
    str_time = datetime.strptime(str_time, ("%H:%M"))
    time = str_time + timedelta(minutes=int_minute)
    out = time.strftime("%H:%M")
    return out


# Transport
def place_to_trip(list_visited_place, transport_type):
    price_norm_all = 0
    price_max_all = 0
    tmp_clock = ""
    order = 0
    # Image for transport
    out = []
    for i in range(len(list_visited_place)-1):
        # init var needed
        tmp_obj = {}
        tmp_trip = {}
        str_place = ""
        init_time = ""
        # Call Tom Tom API
        if i == 0:
            lat1 = list_visited_place[i]['pos_lat']
            lon1 = list_visited_place[i]['pos_lon']
            str_place = 'init'
            init_time = clock_24h_from_12(list_visited_place[i]['jam_init'])
        else:
            lat1 = str(list_visited_place[i].pos_lat)
            lon1 = str(list_visited_place[i].pos_lon)
            str_place = list_visited_place[i].__str__().split(':')[0]
            init_time = tmp_clock
        lat2 = str(list_visited_place[i+1].pos_lat)
        lon2 = str(list_visited_place[i+1].pos_lon)
        tmp_trip_time_dist = get_distance_time_manual(lat1, lon1, lat2, lon2, transport_type)
        trip_time_min = int((int(tmp_trip_time_dist['time_sec'])//60))
        distance_meter = tmp_trip_time_dist['distance_meter']

        # Add to out - obj
        tmp_obj['type'] = str_place
        tmp_obj['objects'] = list_visited_place[i]
        # Init
        if str_place == "init":
            tmp_obj['initial_time'] = init_time
        elif(str_place == "Restoran" or str_place == "Tempat Wisata"):
            # Selain init dan hotel
            tmp_obj['initial_time'] = tmp_clock
            tmp_obj['end_time'] = add_time_to_clock(tmp_clock, int(list_visited_place[i].time_minute))
            tmp_clock = tmp_obj['end_time']
            if str_place == "Tempat Wisata":
                price = list_visited_place[i].price_min
                price_max = list_visited_place[i].price_max
                price_diff = price_max - price
                price_norm_all += price
                price_max_all += price_diff
                tmp_obj['price_min'] = price
                tmp_obj['price_max'] = price_max
            else:
                price = list_visited_place[i].price
                tmp_obj['price'] = price
                price_norm_all += price
        tmp_obj['order'] = order
        order += 1
        out.append(tmp_obj)

        # Add to out - trip
        tmp_trip['type'] = 'transportation'
        tmp_trip['minute'] = trip_time_min
        price = fare_transport(transport_type, distance_meter)
        distance_km = distance_meter/1000
        distance_km = "%.1f" % distance_km
        tmp_trip['distance'] = distance_km
        if i == 0:
            tmp_trip['initial_time'] = init_time
        else:
            tmp_trip['initial_time'] = tmp_clock
        tmp_trip['end_time'] = add_time_to_clock(tmp_trip['initial_time'], trip_time_min)
        tmp_clock = tmp_trip['end_time']
        tmp_trip['price'] = price
        price_norm_all += price
        tmp_trip['order'] = order
        order += 1
        out.append(tmp_trip)
        # print(i)
        # print(out)
    # Last Place
    tmp_obj = {}
    x = len(list_visited_place)-1
    str_place = list_visited_place[x].__str__().split(':')[0]
    # Add to out - obj
    tmp_obj['type'] = str_place
    # print(str_place)
    tmp_obj['objects'] = list_visited_place[x]
    if(str_place == "Restoran" or str_place == "Tempat Wisata"):
        # Selain init dan hotel
        tmp_obj['initial_time'] = tmp_clock
        tmp_obj['end_time'] = add_time_to_clock(tmp_clock, int(list_visited_place[x].time_minute))
        tmp_clock = tmp_obj['end_time']
        if str_place == "Tempat Wisata":
            price = list_visited_place[x].price_min
            price_max = list_visited_place[x].price_max
            price_diff = price_max - price
            price_norm_all += price
            price_max_all += price_diff
            tmp_obj['price_min'] = price
            tmp_obj['price_max'] = price_max
        else:
            price = list_visited_place[x].price
            tmp_obj['price'] = price
            price_norm_all += price
    elif(str_place == "Hotel"):
        # Hotel
        tmp_obj['initial_time'] = tmp_clock
        tmp_obj['end_time'] = '09:00'
        tmp_clock = tmp_obj['end_time']
        price = list_visited_place[x].price
        tmp_obj['price'] = price
        price_norm_all += int(price)
    tmp_obj['order'] = order
    order += 1
    out.append(tmp_obj)

    # last item
    last_item = {}
    last_item['type'] = 'final-item'
    last_item['order'] = order
    out.append(last_item)
    # Tambah max price dan price lalu digabung dengan trip
    all_out = []
    all_out.append(out)
    all_out.append(add_dot(price_norm_all))
    price_max_all = price_max_all + price_norm_all
    all_out.append(add_dot(price_max_all))
    return all_out
