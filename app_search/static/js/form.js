$("#seeAnotherField").change(function() {
    if ($(this).val() == "yes") {
      $('#otherFieldDiv').show();
      $('#otherField').attr('required', '');
      $('#otherField').attr('data-error', 'This field is required.');
    } else {
      $('#otherFieldDiv').hide();
      $('#otherField').removeAttr('required');
      $('#otherField').removeAttr('data-error');
    }
  });
  $("#seeAnotherField").trigger("change");
  
  $("#startPoint").change(function() {
    $('#latInit').removeAttr('required');
    $('#latInit').removeAttr('data-error');
    $('#longInit').removeAttr('required');
    $('#longInit').removeAttr('data-error');
    $('#latInit').removeAttr('readonly', 'True');
    $('#longInit').removeAttr('readonly', 'True');
    $('#latInit').val('');
    $('#longInit').val('');
    if ($(this).val() == "Grand Indonesia") {
      $('#latLongGroup').show();
      $('#latInit').val('-6.1950');
      $('#latInit').attr('readonly', 'True');
      $('#longInit').val('106.8198');
      $('#longInit').attr('readonly', 'True');
    } else if($(this).val() == "Gandaria City") {
      $('#latLongGroup').show();
      $('#latInit').val('-6.2442');
      $('#latInit').attr('readonly', 'True');
      $('#longInit').val('106.7835');
      $('#longInit').attr('readonly', 'True');
    } else if($(this).val() == "Mall Artha Gading") {
      $('#latLongGroup').show();
      $('#latInit').val('-6.1457');
      $('#latInit').attr('readonly', 'True');
      $('#longInit').val('106.8923');
      $('#longInit').attr('readonly', 'True');
    } else if($(this).val() == "Tamini Square") {
      $('#latLongGroup').show();
      $('#latInit').val('-6.2911');
      $('#latInit').attr('readonly', 'True');
      $('#longInit').val('106.8817');
      $('#longInit').attr('readonly', 'True');
    } else if($(this).val() == "Mall Central Park") {
      $('#latLongGroup').show();
      $('#latInit').val('-6.1777');
      $('#latInit').attr('readonly', 'True');
      $('#longInit').val('106.7911');
      $('#longInit').attr('readonly', 'True');
    } else if($(this).val() == "Lippo Mall Puri") {
      $('#latLongGroup').show();
      $('#latInit').val('-6.188011');
      $('#latInit').attr('readonly', 'True');
      $('#longInit').val('106.738755');
      $('#longInit').attr('readonly', 'True');
    } else if($(this).val() == "Titik yang Anda Tentukan") {
      $('#latLongGroup').show();
      $('#latInit').attr('required', '');
      $('#latInit').attr('data-error', 'This field is required.');
      $('#longInit').attr('required', '');
      $('#longInit').attr('data-error', 'This field is required.');
    } else {
      $('#latLongGroup').hide();
      $('#latInit').removeAttr('required');
      $('#latInit').removeAttr('data-error');
      $('#longInit').removeAttr('required');
      $('#longInit').removeAttr('data-error');
    }
  });
  $("#startPoint").trigger("change");
  

$('#datetimepicker4').datetimepicker({
  format: 'L'
});
$('#initTime').datetimepicker({
  format: 'LT'
});
$('#maxTime').datetimepicker({
  format: 'LT'
});