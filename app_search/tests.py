from django.test import TestCase, Client
from .views import (
    clock_24h_from_12, euclidian_distance_meter,
    get_day_from_date_indonesia,
    fare_transport, get_distance_time,
)
import json


class app_searchUnitTest(TestCase):
    fixtures = [
        'dummydata.json',
    ]

    def test_search_page_exist(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_negative_search(self):
        response = Client().get('/search/lalaalla')
        self.assertEqual(response.status_code, 404)

    def test_clock(self):
        expected = "22:30"
        clock = "10:30 PM"
        new_clock = clock_24h_from_12(clock)
        self.assertEqual(new_clock, expected)

    def test_euclidean(self):
        lat1 = 52.2296756
        lon1 = 21.0122287
        lat2 = 52.406374
        lon2 = 16.9251681
        dist = euclidian_distance_meter(lat1, lon1, lat2, lon2)
        expected = 278545
        self.assertEqual(dist, expected)

    def test_day_minggu(self):
        date = "04/21/2019"
        expected_return = {'nama': 'Minggu', 'id': 7}
        out = get_day_from_date_indonesia(date)
        self.assertEqual(out, expected_return)

    def test_day_sabtu(self):
        date = "04/20/2019"
        expected_return = {'nama': 'Sabtu', 'id': 6}
        out = get_day_from_date_indonesia(date)
        self.assertEqual(out, expected_return)

    def test_day_jumat(self):
        date = "04/19/2019"
        expected_return = {'nama': 'Jumat', 'id': 5}
        out = get_day_from_date_indonesia(date)
        self.assertEqual(out, expected_return)

    def test_day_kamis(self):
        date = "04/18/2019"
        expected_return = {'nama': 'Kamis', 'id': 4}
        out = get_day_from_date_indonesia(date)
        self.assertEqual(out, expected_return)

    def test_day_rabu(self):
        date = "04/17/2019"
        expected_return = {'nama': 'Rabu', 'id': 3}
        out = get_day_from_date_indonesia(date)
        self.assertEqual(out, expected_return)

    def test_day_selasa(self):
        date = "04/16/2019"
        expected_return = {'nama': 'Selasa', 'id': 2}
        out = get_day_from_date_indonesia(date)
        self.assertEqual(out, expected_return)

    def test_day_senin(self):
        date = "04/15/2019"
        expected_return = {'nama': 'Senin', 'id': 1}
        out = get_day_from_date_indonesia(date)
        self.assertEqual(out, expected_return)

    def test_fare_gojek(self):
        transport_type = "gojek"
        dist = 11235
        expected_fare = 25000
        fare = fare_transport(transport_type, dist)
        self.assertEqual(fare, expected_fare)

    def test_fare_gocar(self):
        transport_type = "gocar"
        dist = 11235
        expected_fare = 40000
        fare = fare_transport(transport_type, dist)
        self.assertEqual(fare, expected_fare)

    def test_fare_taxi(self):
        transport_type = "taxi"
        dist = 11235
        expected_fare = 53000
        fare = fare_transport(transport_type, dist)
        self.assertEqual(fare, expected_fare)

    def test_get_distance_time_tomtom(self):
        lat_init = -6.3192632
        long_init = 106.8757177
        lat_end = -6.1784541
        long_end = 106.8307066
        response = get_distance_time(lat_init, long_init, lat_end, long_end)
        str_out = json.dumps(response)
        self.assertIn('time_sec', str_out)

    def test_search_plan_with_gojek_and_no_hotel(self):
        response = Client().get('/search/')
        while(('price' not in response.context) or response.context['price'] == '0'):
            response = Client().post('/search/submit/',
                                     {
                                        'startPoint': 'Gandaria City',
                                        'lat-init': '-6.2442',
                                        'long-init': '106.7835',
                                        'seeAnotherField': 'no',
                                        'otherField': '3,4,5',
                                        'tanggal': '05/15/2019',
                                        'jam-awal': '10:00 AM',
                                        'jam-akhir': '11:00 AM',
                                        'transport': 'gojek'}
                                     )
        self.assertEqual(str(response.context['input']['transport']), "gojek")
        self.assertEqual(str(response.context['trips'][2]['objects']), "Tempat Wisata: Pasar Santa")

    def test_search_plan_with_taxi_and_hotel(self):
        response = Client().get('/search/')
        while(('price' not in response.context) or response.context['price'] == '0'):
            response = Client().post('/search/submit/',
                                     {
                                        'startPoint': 'Titik yang Anda Tentukan',
                                        'lat-init': '-6.362764',
                                        'long-init': '106.827049',
                                        'seeAnotherField': 'yes',
                                        'otherField': '3,4,5',
                                        'tanggal': '05/15/2019',
                                        'jam-awal': '10:00 AM',
                                        'jam-akhir': '10:00 PM',
                                        'transport': 'taxi'}
                                     )
        self.assertEqual(str(response.context['input']['transport']), "taxi")
        self.assertEqual(str(response.context['trips'][-2]['type']), "Hotel")

    def test_search_plan_with_gocar(self):
        response = Client().get('/search/')
        while(('price' not in response.context) or response.context['price'] == '0'):
            response = Client().post('/search/submit/',
                                     {
                                        'startPoint': 'Titik yang Anda Tentukan',
                                        'lat-init': '-6.263739',
                                        'long-init': '106.6595262',
                                        'seeAnotherField': 'yes',
                                        'otherField': '3,4,5',
                                        'tanggal': '05/05/2019',
                                        'jam-awal': '10:00 AM',
                                        'jam-akhir': '11:59 PM',
                                        'transport': 'gocar'}
                                     )
        self.assertEqual(str(response.context['input']['transport']), "gocar")

    def test_attraction_is_closed(self):
        for i in range(5):
            # Taman Ismail Marzuki (Tutup Senin)
            response = Client().post('/search/submit/',
                                     {
                                        'startPoint': 'Titik yang Anda Tentukan',
                                        'lat-init': '-6.1915046',
                                        'long-init': '106.8362920',
                                        'seeAnotherField': 'no',
                                        'otherField': '3,4,5',
                                        'tanggal': '05/13/2019',
                                        'jam-awal': '08:00 AM',
                                        'jam-akhir': '11:01 AM',
                                        'transport': 'gocar'}
                                     )
            self.assertNotEqual(str(response.context['trips'][-2]['objects']),
                                "Tempat Wisata: Taman Ismail Marzuki")

    def test_restaurant(self):
        for i in range(5):
            # Taman Ismail Marzuki (Tutup Senin)
            response = Client().post('/search/submit/',
                                     {
                                        'startPoint': 'Titik yang Anda Tentukan',
                                        'lat-init': '-6.263740',
                                        'long-init': '106.6595280',
                                        'seeAnotherField': 'no',
                                        'otherField': '3,4,5',
                                        'tanggal': '05/05/2019',
                                        'jam-awal': '10:45 AM',
                                        'jam-akhir': '02:15 PM',
                                        'transport': 'gocar'}
                                     )
            self.assertNotEqual(str(response.context['trips'][-2]['objects']),
                                "Restoran: Bebek Kaleyo")

    def test_restaurant_not_found(self):
        # Taman Ismail Marzuki (Tutup Senin)
        response = Client().post('/search/submit/',
                                 {
                                    'startPoint': 'Titik yang Anda Tentukan',
                                    'lat-init': '51.404503',
                                    'long-init': '30.054234',
                                    'seeAnotherField': 'no',
                                    'otherField': '3,4,5',
                                    'tanggal': '05/05/2019',
                                    'jam-awal': '10:45 AM',
                                    'jam-akhir': '02:15 PM',
                                    'transport': 'gocar'}
                                 )
        self.assertEqual(str(response.context['trips'][-2]['type']), "{'start_point'")

    def test_search_plan_for_lunch(self):
        response = Client().get('/search/')
        while(('price' not in response.context) or response.context['price'] == '0'):
            response = Client().post('/search/submit/',
                                     {
                                        'startPoint': 'Titik yang Anda Tentukan',
                                        'lat-init': '-6.263740',
                                        'long-init': '106.6595280',
                                        'seeAnotherField': 'no',
                                        'otherField': '3,4,5',
                                        'tanggal': '05/13/2019',
                                        'jam-awal': '10:45 AM',
                                        'jam-akhir': '01:15 PM',
                                        'transport': 'gocar'}
                                     )
        self.assertEqual(str(response.context['trips'][-2]['type']), "Restoran")

    def test_search_plan_with_skip_dinner(self):
        response = Client().get('/search/')
        while(('price' not in response.context) or response.context['price'] == '0'):
            response = Client().post('/search/submit/',
                                     {
                                        'startPoint': 'Titik yang Anda Tentukan',
                                        'lat-init': '-6.1929475',
                                        'long-init': '106.8199218',
                                        'seeAnotherField': 'no',
                                        'otherField': '3,4,5',
                                        'tanggal': '05/13/2019',
                                        'jam-awal': '05:14 PM',
                                        'jam-akhir': '06:45 PM',
                                        'transport': 'gocar'}
                                     )
        self.assertNotEqual(str(response.context['trips'][-2]['type']), "Restoran")

    def test_submit_redirect_to_form(self):
        response = Client().get('/search/submit', follow=True)
        self.assertEqual(response.redirect_chain[-1][0], '/search/')
