from django.test import TestCase, Client
from .models import Day, Hotel, Restaurant, TempatWisata

# Create your tests here.


def create_day(nama_hari):
    d = Day.objects.create(day_name=nama_hari)
    return d


def create_hotel(nama):
    h = Hotel.objects.create(name=nama, price='1000', pos_lat='1.0', pos_lon='1.0', img_URL='test_url',
                             address='alamat', desc='deskripsi', hotel_star=5, is_approved=True)
    return h


def create_tempat_wisata(nama):
    tw = TempatWisata.objects.create(name=nama, price_min='1000', price_max='1500', pos_lat='1.0', pos_lon='1.0',
                                     img_URL='test_url', address='alamat', desc='deskripsi', time_minute=60,
                                     weekend_from_hour=8, weekend_to_hour=17, weekday_from_hour=9,
                                     weekday_to_hour=20, is_approved=True)
    return tw


def create_restoran(nama):
    r = Restaurant.objects.create(name=nama, price='1000', pos_lat='1.0', pos_lon='1.0',
                                  img_URL='test_url', address='alamat', desc='deskripsi', time_minute=60,
                                  weekend_from_hour=8, weekend_to_hour=17, weekday_from_hour=9,
                                  weekday_to_hour=20, is_approved=True)
    return r


class app_homeUnitTest(TestCase):

    def test_landing_page_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_negative_landing(self):
        response = Client().get('/home/lalaalla')
        self.assertEqual(response.status_code, 404)


class DataHomeTest(TestCase):
    def setUp(Self):
        create_day('Senin')
        create_hotel('hotelku')
        create_restoran('mcd')
        create_tempat_wisata('mekarsari')

    def test_all_day(self):
        all_day = Day.objects.all().count()
        self.assertEqual(all_day, 1)

    def test_get_data_from_day(self):
        d = Day.objects.get(day_name="Senin")
        self.assertEqual("Senin", str(d))

    def test_all_hotel(self):
        all_hotel = Hotel.objects.all().count()
        self.assertEqual(all_hotel, 1)

    def test_get_data_from_hotel(self):
        h = Hotel.objects.get(name="hotelku")
        self.assertEqual("Hotel: hotelku 5", str(h))

    def test_all_resto(self):
        all_resto = Restaurant.objects.all().count()
        self.assertEqual(all_resto, 1)

    def test_get_data_from_resto(self):
        r = Restaurant.objects.get(name="mcd")
        self.assertEqual("Restoran: mcd", str(r))

    def test_all_attraction(self):
        all_tempat_wisata = TempatWisata.objects.all().count()
        self.assertEqual(all_tempat_wisata, 1)

    def test_get_data_from_tempat_wisata(self):
        tw = TempatWisata.objects.get(name="mekarsari")
        self.assertEqual("Tempat Wisata: mekarsari", str(tw))
