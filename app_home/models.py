from django.db import models
from django.utils import timezone

HOUR_OF_DAY_24 = [(i, i) for i in range(1, 25)]


class Day(models.Model):
    day_name = models.CharField(max_length=50)
    created_at = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField(editable=False)

    def __str__(self):
        return self.day_name

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.modified_at = timezone.now()
        return super(Day, self).save(*args, **kwargs)


class Hotel(models.Model):
    name = models.CharField(max_length=50, unique=True)
    price = models.CharField(max_length=40)
    pos_lat = models.FloatField()
    pos_lon = models.FloatField()
    img_URL = models.TextField()
    address = models.TextField()
    desc = models.TextField()
    created_at = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField(editable=False)
    is_approved = models.BooleanField(default=False)
    hotel_star = models.IntegerField()
    list_per_page = 20

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.modified_at = timezone.now()
        return super(Hotel, self).save(*args, **kwargs)

    def __str__(self):
        return "Hotel: " + self.name + " " + str(self.hotel_star)


class Restaurant(models.Model):
    name = models.CharField(max_length=50, unique=True)
    price = models.IntegerField()
    pos_lat = models.FloatField()
    pos_lon = models.FloatField()
    img_URL = models.TextField()
    address = models.TextField()
    desc = models.TextField()
    created_at = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField(editable=False)
    is_approved = models.BooleanField(default=False)
    time_minute = models.IntegerField()
    weekend_from_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    weekend_to_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    weekday_from_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    weekday_to_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    closed_day = models.ManyToManyField(Day, blank=True)
    list_per_page = 20

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.modified_at = timezone.now()
        return super(Restaurant, self).save(*args, **kwargs)

    def __str__(self):
        return "Restoran: " + self.name


class TempatWisata(models.Model):
    name = models.CharField(max_length=50, unique=True)
    price_min = models.IntegerField()
    price_max = models.IntegerField()
    pos_lat = models.FloatField()
    pos_lon = models.FloatField()
    img_URL = models.TextField()
    address = models.TextField()
    desc = models.TextField()
    created_at = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField(editable=False)
    is_approved = models.BooleanField(default=False)
    time_minute = models.IntegerField()
    weekend_from_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    weekend_to_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    weekday_from_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    weekday_to_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    closed_day = models.ManyToManyField(Day, blank=True)
    list_per_page = 20

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.modified_at = timezone.now()
        return super(TempatWisata, self).save(*args, **kwargs)

    def __str__(self):
        return "Tempat Wisata: " + self.name
