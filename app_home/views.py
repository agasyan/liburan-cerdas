from django.shortcuts import render

# Create your views here

response = {}


def index(request):
    response.clear()
    response['pageTitle'] = 'Home'
    response['activeHome'] = 'active'
    return render(request, 'index.html', {'response': response})
