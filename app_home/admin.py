from django.contrib import admin
from .models import Day, TempatWisata, Hotel, Restaurant
from import_export.admin import ImportExportModelAdmin


class RestaurantAdmin(ImportExportModelAdmin):
    list_display = ['id', 'name', 'is_approved', 'modified_at', 'created_at']
    search_fields = ['name']


class TempatWisataAdmin(ImportExportModelAdmin):
    list_display = ['id', 'name', 'is_approved', 'modified_at', 'created_at']
    search_fields = ['name']


class HotelAdmin(ImportExportModelAdmin):
    list_display = ['id', 'name', 'hotel_star', 'is_approved', 'modified_at', 'created_at']
    search_fields = ['name']


class DayAdmin(ImportExportModelAdmin):
    list_display = ['id', 'day_name', 'modified_at', 'created_at']
    search_fields = ['day_name']


admin.site.register(Day, DayAdmin)
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Hotel, HotelAdmin)
admin.site.register(TempatWisata, TempatWisataAdmin)
